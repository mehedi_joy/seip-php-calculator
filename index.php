<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Calculator</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
</head>

<body>
    <?php 
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $number1 = $_POST["numberOne"];
            $number2 = $_POST["numberTwo"];
            $operation = $_POST["operation"];
            $res = null;
            $flag = false;
            if($number1 == null || $number2 == null) {
                $flag = !$flag;
            } else {
                switch($operation) {
                    case "add":
                        $res = $number1 + $number2;
                        break;
                    case "sub":
                        $res = $number1 - $number2;
                        break;
                    case "mul":
                        $res = $number1 * $number2;
                        break;
                    case "div":
                        $res = $number1 / $number2;
                        break;
                }
            }   
        }
    ?>
    <div class="container mt-5" style="width: 30vw;">
        <form action="index.php" method="POST">
            <div class="mb-3">
                <input class="form-control" type="number" name="numberOne" id="numberOne" placeholder="Please Enter Number One">
            </div>
            <div class="mb-3">
                <input class="form-control" type="number" name="numberTwo" id="numberTwo" placeholder="Please Enter Number Two">
            </div>
            <?php if($_SERVER["REQUEST_METHOD"] == "POST" && !$flag): ?>
            <h3> = <?php echo $res ?></h3>
            <?php elseif($_SERVER["REQUEST_METHOD"] == "POST" && $flag): ?>
            <h3>Please Enter Valid Numbers.</h3>
            <?php endif ?>
            <button class="btn btn-primary" style="padding: 10px 30px;font-size: 1.5rem" type="submit" name="operation" id="add" value="add">+</button>
            <button class="btn btn-primary" style="padding: 10px 30px;font-size: 1.5rem" type="submit" name="operation" id="sub" value="sub">-</button>
            <button class="btn btn-primary" style="padding: 10px 30px;font-size: 1.5rem" type="submit" name="operation" id="mul" value="mul">x</button>
            <button class="btn btn-primary" style="padding: 10px 30px;font-size: 1.5rem" type="submit" name="operation" id="div" value="div">÷</button>
        </form>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</body>

</html>